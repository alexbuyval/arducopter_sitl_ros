This repository is ROS package for SITL mode of ArduCopter.

Please note, now you should use my [fork of Ardupilot](https://github.com/alexbuyval/ardupilot.git) firmware to use this package.

## Install ##
* Install ROS and so on
* Clone my fork of Ardupilot

```
#!bash

git clone https://github.com/alexbuyval/ardupilot.git
git checkout RangeFinderSITL 
```
* Clone this repository in your catkin workspace

```
#!bash

git clone https://bitbucket.org/alexbuyval/arducopter_sitl_ros
```
* Clone my fork of 'rotors_simulator' in your catkin workspace

```
#!bash

git clone https://github.com/alexbuyval/rotors_simulator.git
git checkout sonar_plugin
```
* Clone my fork of 'ar_track_alvar' in your catkin workspace

```
#!bash

git clone https://github.com/alexbuyval/ar_track_alvar.git
```
* Clone my fork of 'mavros' in your catkin workspace

```
#!bash

git clone https://github.com/alexbuyval/mavros
```

## Run ##

Run the following command from ArduCopter directory:
```
#!bash

sim_vehicle.sh -f arducopter_sitl_ros --console
```